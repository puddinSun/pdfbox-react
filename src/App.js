import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {

  // load balancer
  const METADATA_URL = "http://localhost:3000/api/v1/file/metadata"
  const DOWNLOAD_URL = "http://localhost:3000/api/v1/file/download"

  const downloadFile = () => {
    axios(METADATA_URL, { method: 'GET' }).then(response => {
      const data = response.data;
      return { fileName: data.fileName, extension: data.extension }
    }).then((metaData) => {
      const requestOptions = {
        method: 'POST',
        responseType: 'blob', // important
        data: {
          name: "Dummy Name",
          accountNumber: null,
          loanAmount: 2400.00,
          effectiveInterestRate: 1.3,
          notarialFee: null,
          docStampFee: null,
          processingFee: 12,
          prepaidInterest: 1.2,
          netDisbursementAmount: 1000,
          disbursementAccountNumber: "12343535343",
          monthlyRepayment: 200,
          aor: 200,
          firstDueDate: "20221210",
          maturityDate: "20221210"
        }
      };
      axios(DOWNLOAD_URL, requestOptions)
        .then(response => {
          const href = window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
          const link = document.createElement('a');
          link.href = href;
          link.setAttribute('download', metaData.fileName + "." + metaData.extension);
          link.click();
          window.URL.revokeObjectURL(href);
        })
    })
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a onClick={downloadFile}>Download loan offer</a>
      </header>
    </div>
  );
}

export default App;
