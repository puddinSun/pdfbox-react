FROM node:18-alpine as buildStage
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY public ./public
COPY src ./src
RUN yarn run build

FROM nginx:alpine
COPY --from=buildStage /app/build /usr/share/nginx/html
EXPOSE 80
# Start nginx
CMD ["nginx", "-g", "daemon off;"]